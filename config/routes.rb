Rails.application.routes.draw do
  devise_for :users
  resources :courses
	root 'static_pages#homepage'
	# get 'static_pages/homepage'
	# get 'static_pages/privacy'
	get 'privacy', to: 'static_pages#privacy'
	get 'static_pages/activity'
end
