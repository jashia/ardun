class ApplicationController < ActionController::Base
	before_action :authenticate_user!
	# add to the activity tracker on the App Controller level
	include PublicActivity::StoreController
end
